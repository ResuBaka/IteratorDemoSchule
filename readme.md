##Definiton

Bei Iterator geht es um Iteration. Es geht darum das man über das Objekt Iterieren kann.

##Einsetztgebiet

Iteration über objekte.

##Funktion

![alt text](https://i.imgur.com/2INNyMd.png)

##Beispiel [Quellcode](https://gitlab.com/ResuBaka/IteratorDemoSchule)

Um dies zu Testen kann der Code in die Console der Entwicklungsumgebung vom Browser kopiert werden und aus geführt werden.

Oder man hat Nodejs installiert und macht ``node index.js``

```javascript
let Array = ['a', 'b', 'c'];
let Iterator = Array[Symbol.iterator]();

console.log(Iterator.next());
console.log(Iterator.next());
console.log(Iterator.next());
console.log(Iterator.next());
```
    Ausgabe: 
    { value: 'a', done: false }
    { value: 'b', done: false }
    { value: 'c', done: false }
    { value: undefined, done: true }